Meteor.startup(function() {
  setupCollections();
  setupLibraries();
  startParsing();
});

setupCollections = function() {
  Articles = new Mongo.Collection("articles");

  Meteor.publish("articles", function(limit) {
    return Articles.find({}, 
      {
        sort: {
          dateTime: -1
        }
      ,
      limit: limit});
  });

  Articles._ensureIndex({
    "dateTime": 1
  });
  Articles._ensureIndex({
    "type": 1,
    "dateTime": 1
  });
}

setupLibraries = function() {
  Request = Meteor.npmRequire("request");
  Cheerio = Meteor.npmRequire('cheerio');
  Fiber = Meteor.npmRequire('fibers');
}
