var debug = false;
var seconds = debug ? 30 : 10;
var interval = 1000 * seconds;


startParsing = function() {
  if (debug) {
    fire_parsers()
  }
  setInterval(function() {
    fire_parsers()
  }, interval);
}

function fire_parsers() {
  parseSloTech();
  parseRacunalniskeNovice();
  parseGizzmo();
  parseSloAndroid();
  parse24ur();
}
