// ========================================= CONSTANTS =========================================
var monthNames = {
  english: ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ],
  slovenian: ["januar", "februar", "marec", "april", "maj", "junij",
    "julij", "avgust", "september", "oktober", "november", "december"
  ]
};
// =============================================================================================

// ======================================= HELPER METHODS ======================================
cleanString = function cleanString(text) {
  return text.replace(/(\r\n|\n|\t|\t|\Z\r)/gm, "").trim();
};

getMonthIndexByName = function getMonthIndexByName(month_name, language) {
  return monthNames[language].indexOf(month_name)
};

asyncLoop = function asyncLoop(o) {
  var i = -1;

  var loop = function() {
    i++;
    if (i == o.length) {
      o.callback();
      return;
    }
    o.functionToLoop(loop, i);
  }
  loop(); //init
};
// =============================================================================================

generateSummary = function generateSummary(bodyText) {
  var minimumSize = 400;
  if (bodyText.length <= minimumSize) {
    return bodyText;
  } else {
    for (var i = minimumSize; i < bodyText.length; i++) {
      if (bodyText.charAt(i) == ' ') {
        return bodyText.substring(0, i).concat(' ...');
      }
    }
    return bodyText;
  }
};

CleanDB = function() {
  console.log("Clean all articles");
  Articles.remove({});
};
