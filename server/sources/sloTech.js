parseSloTech = function() {
  try {
    Request('https://slo-tech.com', function(error, response, body) {
      if (!error && response.statusCode == 200) {
        $ = Cheerio.load(body);
        links = $('#fresh_news li a')
        links.each(function(index) {
          var link = 'https://slo-tech.com'.concat($(this).attr('href'));
          Request(link, function(error, response, body) {
            if (!error && response.statusCode == 200) {
              $ = Cheerio.load(body);
              var article = {};
              article.createdAt = new Date();
              article.type = "SLO_TECH";
              article.title = $('[itemprop=name]').text();
              if (!article.title) {
                return;
              }
              article.dateTime = new Date($('[itemprop=datePublished]').attr('datetime'));
              article.author = $('[itemprop=author]').text();
              article.images = [];
              $('article img').each(function(index) {
                var src = $(this).attr('src');
                var title = $(this).attr('alt');
                if (src) {
                  article.images.push({
                    "title": title,
                    "src": src.replace('sm.jpg', '.jpg')
                  });
                }
              });
              article.videos = [];
              $('.youtube-player').each(function(index) {
                article.videos.push($(this).attr('src'));
              });

              article.sources = [];
              $('.source').each(function(index) {
                var title = $(this).text();
                var href = $(this).attr('href');
                if (title && href) {
                  article.sources.push({
                    "title": title,
                    "href": href
                  });
                }
              });
              $('.besediloNovice div').remove();
              $('.besediloNovice iframe').remove();
              $('.besediloNovice .source').remove();
              article.html = $('.besediloNovice').html().substring(3).trim();
              article.summary = generateSummary($('.besediloNovice').text().substring(3));
              Fiber(function() {
                Meteor.call("addArticle", article);
              }).run();
            }
          });
        });
      }
    });
  } catch (e) {
    console.error(e);
  }
}
