 parseSloAndroid = function parseSloAndroid() {
   console.log("Parse SloAndroid!");
   try {
     var current_page = 0;
     var num_pages = 30;
     var step = 13;
     var base_link = "http://slo-android.si"

     asyncLoop({
       length: num_pages,
       functionToLoop: function(loop, current_page) {
         Request(base_link + '/prispevki/novice.html?start=' + current_page * step, function(error, response, body) {
           if (!error && response.statusCode == 200) {
             $ = Cheerio.load(body);
             $('.catItemTitle a').each(function(index) {
               console.log("PARSING URL: " + base_link + $(this).attr('href'));
               parse_site(base_link + $(this).attr('href'))
             });
           }
         });
         loop();
       },
       callback: function() {
         console.log('All done!');
       }
     });
   } catch (e) {
     console.log('ERROR PARSING SloAndroid!');
     console.error(e);
   }
 };

 function parse_site(link) {
   Request(link, function(error, response, body) {
     if (!error && response.statusCode == 200) {
       $ = Cheerio.load(body);
       header_block = $('.itemHeader')
       author_block = header_block.find('.itemAuthor')
       author = author_block.find('a').first().text()
       author_block.find('a').remove()

       var article = {
         createdAt: new Date(),
         type: "SLO_ANDROID",
         title: cleanString(header_block.find('h2.itemTitle').text()),
         dateTime: parseDate(author_block),
         author: cleanString(author)
       };
       if (!article.title) {
         return;
       }

       article.images = [];
       $('.itemImage img').each(function(index) {
         var src = $(this).attr('src');
         var title = $(this).attr('alt');
         if (src) {
           article.images.push({
             "title": title,
             "src": src
           });
         }
       });
       article.html = $('.itemFullText').html();
       article.summary = generateSummary(cleanString($('.itemFullText').text()));
       Fiber(function() {
         Meteor.call("addArticle", article);
       }).run();

     }
   });
 };

 function parseDate(date_object) {
   date = cleanString(date_object.text()).split('::')[1].split(' ')
   hour_minutes = date[5].split(':')
   return new Date(date[3], getMonthIndexByName(date[2], 'slovenian'), date[1].slice(0, -1), hour_minutes[0], hour_minutes[1])
 };
