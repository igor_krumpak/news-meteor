 parseGizzmo = function parseGizzmo() {
   //console.log("Parse Gizzmo.si!!!");
   try {
     Request('https://gizzmo.si/blog/', function(error, response, body) {
       if (!error && response.statusCode == 200) {
         $ = Cheerio.load(body);
         $('.article-card.square').each(function() {
           var url = $(this).find('.inner').find('a').attr('href');
           //console.log('URL: '+url);
           var req = Request(url, function(error, response, body) {
             if (!error && response.statusCode == 200) {
               $ = Cheerio.load(body);
               var article = {};
               article.images = [];
               article.createdAt = new Date();
               article.dateTime = new Date();

               article.type = "GIZZMO";
               article.title = $('h1 a').attr("title");
               article.author = $('span.author a').text();

               var src = $('.article-card').find('div.image-background').css('background-image');
               if (src) {
                 src = src.replace('url(\'', '');
                 src = src.replace('\')', '');
                 article.images.push({
                   "title": 'Empty',
                   "src": src
                 });
               }

               $('.entry-content').children().each(function() {
                 if ($(this).is('p') || $(this).is('ul'))
                   article.html += $(this).html();
                 else if ($(this).is('img')) {
                   var imgLink = $(this).attr('src');
                   var imgTitle = $(this).attr('alt');
                   article.images.push({
                     "title": imgTitle,
                     "src": imgLink
                   });
                 }
               });

               article.summary = generateSummary($('.entry-content p:first-child').text());
               Fiber(function() {
                 Meteor.call("addArticle", article);
               }).run();
             }
           });
         });
       }
     });
   } catch (e) {
     console.log('ERROR WHILE PARSING GIZZMO!!!');
     console.error(e);
   }
 }
