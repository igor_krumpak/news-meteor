parseRacunalniskeNovice = function() {
  try {
    for (var i = 0; i < 3; i++) {
      var url = "http://www.racunalniske-novice.com/novice/";
      if (i > 0) {
        url = url.concat('listaj-arhiv/').concat(i).concat('/');
      }
      Request(url, function(error, response, body) {
        if (!error && response.statusCode == 200) {
          $ = Cheerio.load(body);
          var links = $('.latest-dotted-nl .holder a:first-child');
          links.each(function(index) {
            var link = $(this).attr('href');
            Request(link, function(error, response, body) {
              $ = Cheerio.load(body);
              var article = {};
              article.createdAt = new Date();
              article.type = "RACUNALNISKE_NOVICE";
              article.title = $('h1').text();
              var dateTimeRaw = $('.single-art-date.fl.mr10').text().trim().split('      ');
              article.dateTime = moment(dateTimeRaw[0].concat(' ').concat(dateTimeRaw[1]), 'DD.MM.YYYY HH:mm').toDate();
              article.images = [];
              $('#single-art-text img').each(function(index) {
                var src = 'http://www.racunalniske-novice.com/'.concat($(this).attr('src'));
                var title = $(this).attr('alt');
                article.images.push({
                  "title": title,
                  "src": src
                });
              });
              article.sources = [];
              $('.single-art-linkage a').each(function(index) {
                var title = $(this).text();
                var href = $(this).attr('href');
                if (title && href) {
                  article.sources.push({
                    "title": title,
                    "href": href
                  });
                }
              });
              $('.single-art-text div').remove();
              $('.single-art-text script').remove();
              article.html = $('.single-art-text').html().trim();
              article.summary = generateSummary($('.single-art-text').text().trim());
              Fiber(function() {
                Meteor.call("addArticle", article);
              }).run();
            });
          });

        }
      });
    }
  } catch (e) {
    console.error(e);
  }
}
