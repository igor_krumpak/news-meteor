parse24ur = function() {
  console.log("parsing 24 ur");

  try {
    var mainLink = "http://www.24ur.com/novice/it"
    Request(mainLink, function(error, response, body) {
      if (!error && response.statusCode == 200) {
        $ = Cheerio.load(body);
        var news = $(".news-category .item-content h3 a:first-child");
        //console.log(news.length);
        news.each(function(idx) {
          var articleLink = news[idx].attribs.href.slice(10);
          //console.log(articleLink)
          Request(mainLink + articleLink, function(error, response, body) {
            $ = Cheerio.load(body);
            var article = {};
            article.type = "24UR";
            article.createdAt = new Date();
            article.title = $(".article-frame h1").text();
            var dateString = $(".containerLeftSide h3").text().split(",");
            var date = dateString[1];
            article.dateTime = moment(date, 'DD.MM.YYYY').toDate();
            var html = $(".contextual-text p").text().trim();
            article.html = html;

            article.images = [];
            $(".picture").each(function(idx){
              var src = $(this).find("img").attr("src");
              var title = $(this).find("h4").text();
              var image = {
                src: src,
                title: title
              };
              article.images.push(image);
            });

            $(".picturewide").each(function(idx){
              var image = {
                src: $(this).find("img").attr("src"),
                title: $(this).find("h4").text()
              }
              article.images.push(image);
            })

            article.sources = [];
            article.summary = generateSummary($(".contextual-text p").text().trim());
            //console.log(article.title);
            Fiber(function() {
              Meteor.call("addArticle", article);
            }).run();
          })
        })
      }
    })
    console.log("parse24ur FINISHED");
  } catch (e) {
    console.log(e);
  }
}