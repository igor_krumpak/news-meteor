Template.articles.helpers({
  image: function() {
    if (this.images.length > 0) {
      return this.images[0].src;
    }
    return "";
  }
});



Template.articles.rendered = function() {
	var articlesScroll = $('.articles-scroll');
  articlesScroll.scroll(function() {
  	console.log("scroll");
  	if(articlesScroll.scrollTop() + articlesScroll.innerHeight() >= articlesScroll[0].scrollHeight) {
  		console.log("end of page");
  		PaginationArticles.loadNextPage();
  	}
  });
}