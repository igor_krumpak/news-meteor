Router.route('/', function() {
  this.layout('ApplicationLayout', {
    data: {
      title: 'Zadnje novice'
    }
  });
  this.render('articles', {
    data: function() {
      return Articles.find({}, {
        sort: {
          dateTime: -1
        }
      });
    }
  });
});

Router.route('articles/:_id', function() {
  this.layout('ApplicationLayout');
  this.render('article', {
    data: function() {
      return Articles.findOne({
        _id: this.params._id
      });
    }
  });
});

Router.route('articles/type/:type', function() {
  var title = this.params.type.replace("_", " ");
  this.layout('ApplicationLayout', {
    data: {
      title: title
    }
  });
  this.render('articles', {
    data: function() {
      return Articles.find({
        type: this.params.type
      }, {
        sort: {
          dateTime: -1
        }
      });
    }
  });
});
